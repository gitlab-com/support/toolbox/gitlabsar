# GitLabSAR

**GitLabSAR** can be used to quickly search through GitLab logs for specific data.

This is an extremely early iteration, but may be ready for use!

## Installation

You may download compiled binaries or download the source repository and compile it yourself.

### Download binaries

Download and run `gitlabsar` compiled binaries from the CI artifacts. [Download the latest Linux build here](https://gitlab.com/api/v4/projects/42348764/jobs/artifacts/main/download?job=build:stable-x86_64-unknown-linux-gnu).

### Compiling

Install Rust (as with [rustup](https://rustup.rs/)).

Compile and install a release build:

```
cargo install --path .
```

## Usage

Try `gitlabsar search "your regex here"` on a GitLab node. This will do a relatively fast search for the given regex across all of the GitLab logs and compress the raw JSON data on the matches into a file.

For more options, see `gitlabsar help`.

## Background

[GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) is a great tool for gathering general server information and recent logs from a GitLab node. However, it is limited in the amount of data it can capture, otherwise it is difficult for customers to share that information with GitLab Support.

Sometimes we want information that is further back in the logs. Using the commonly-available tools (such as `grep`) to search through gigabytes of logs will take a very long time, and might not even capture all of the context we might want.

Similar to GitLabSOS, GitLabSAR is meant to be run by customers on their GitLab nodes. Instead of gathering recent data, it gets specific information (as specified in command parameters) and useful context surrounding the found information.

### Name

GitLabSAR is a play on GitLabSOS, where SAR stands for Search And Rescue.

## Documentation

API documentation, generated from doc comments with rustdoc, are available on the GitLab Pages site: <https://gitlab-com.gitlab.io/support/toolbox/gitlabsar/gitlabsar/>
