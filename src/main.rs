//! GitLabSAR is an application meant for quickly searching for specific data on a GitLab instance.
//! It is faster than `grep` at searching through files, and gets more context.
//!
//! The top-level types mainly relate to the CLI. Everything else is split into modules.
//!
//! The [commands] module contains the logic for each subcommand. [search] contains general-purpose
//! search functionality, to be used by the commands to find information in the GitLab logs.

use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

use chrono::Utc;
use clap::{Args, Parser, Subcommand, ValueEnum};

mod commands;
mod compress;
mod search;

// I need to find the best way to document this via Cargo, to make it easier for others to
// contribute to the code, while also providing good about sections for the help output in the CLI.
#[derive(Parser)]
#[command(author, version, about, long_about = None, subcommand_required = true)]
struct Cli {
    /// GitLab log path
    #[arg(short, long, global = true, default_value = "/var/log/gitlab")]
    log_path: PathBuf,
    /// Extract and search gzip archives
    #[arg(short, long, global = true, default_value = "false")]
    unzip: bool,
    // Range of dates/times to search
    //#[arg(short, long, global=true, default_value = "-")]
    //range: String,
    // Length of time to search
    //#[arg(short, long, global=true, default_value = "1h")]
    //time: String,
    // Lines of context before and after found lines to include
    //#[arg(short, long, global=true, default_value = 0)]
    //context: u8,
    /// Print raw JSON to stdout instead of writing to a file
    #[arg(short, long, global = true, default_value = "false")]
    stdout: bool,
    /// Don't compress JSON before writing to disk
    #[arg(short = 'C', long, global = true)]
    nocompress: bool,
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Search for a correlation ID
    Search(Search),
    // Search for errors in specific logs
    //Errors(Errors),
}

#[derive(Args)]
pub struct Search {
    #[arg(from_global)]
    log_path: PathBuf,
    /// The regex to search for
    // TODO: Validate correlation IDs
    regex: String,
    /// Restrict search to certain log types
    #[arg(short = 'f', long, value_enum, default_value = "all")]
    log_types: Vec<LogTypes>,
    #[arg(from_global)]
    unzip: bool,
    #[arg(from_global)]
    stdout: bool,
    // TODO: Add compression options
}

#[derive(ValueEnum, Clone)]
pub enum LogTypes {
    All,
    //Rails,
    //Production,
    //Api,
    //Application,
    //Integrations,
    //Git,
    //Audit,
    //Sidekiq,
    //SidekiqCurrent,
    //SidekiqClient,
    //GitlabShell,
    //Gitaly,
    //GitalyCurrent,
    //GitalyGrpc,
    //etc.
}

impl LogTypes {
    fn path(self) -> PathBuf {
        use LogTypes::*;
        match self {
            All => "",
        }
        .into()
    }
}

/// The entrypoint of GitLabSAR, this just passes the arguments on to the correct subcommand, then
/// passes the result to the compression function.
fn main() {
    let args = Cli::parse();

    let search_result = match args.command {
        Commands::Search(search) => commands::search(search),
    }
    .unwrap();

    if args.stdout {
        let result_str = String::from_utf8(search_result).expect("Found invalid UTF-8");
        println!("{}", result_str);
    } else if !args.nocompress {
        compress::compress_result(&search_result).unwrap();
    } else {
        let now = Utc::now();
        let file_name = format!("gitlabsar_{}.json", now.format("%Y%m%d%H%M%S"));
        let mut file = File::create(file_name).unwrap();
        file.write_all(&search_result).unwrap();
    }
}
