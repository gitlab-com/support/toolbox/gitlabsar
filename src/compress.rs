//! Functions for processing and compressing search results.

use std::fs::File;
use std::io::Write;

use anyhow::Error;
use chrono::Utc;
use flate2::write::GzEncoder;
use flate2::Compression;

/// Takes a search result and compresses it into a file.
pub fn compress_result(search_json: &[u8]) -> Result<(), Error> {
    let mut e = GzEncoder::new(Vec::new(), Compression::default());
    e.write_all(search_json)?;
    let compressed_result = e.finish()?;

    let now = Utc::now();
    let file_name = format!("gitlabsar_{}.json.gz", now.format("%Y%m%d%H%M%S"));
    let mut file = File::create(file_name)?;
    file.write_all(&compressed_result)?;

    Ok(())
}
