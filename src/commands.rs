//! The logic for each of GitLabSAR's subcommands.

use super::search;
use anyhow::Error;

/// Search sith a particular regex across any/all logs.
pub fn search(args: super::Search) -> Result<Vec<u8>, Error> {
    // Potential option for only matching json logs?
    let match_bytes = search::search(&args.regex, args.log_types, args.log_path, args.unzip)?;
    Ok(match_bytes)
}
