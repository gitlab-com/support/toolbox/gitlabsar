//! Functions for finding information in GitLab logs.

use std::fs::File;
use std::io::Seek;
use std::path::PathBuf;

use super::LogTypes;
use anyhow::{anyhow, Error};
use flate2::read::GzDecoder;
use grep::{printer::JSON, regex::RegexMatcher, searcher::Searcher};
use walkdir::WalkDir;

/// Search for any regex across specified logs.
///
/// unzip allows for searching through compressed logs.
pub fn search(
    regex: &str,
    logs: Vec<LogTypes>,
    log_path: PathBuf,
    unzip: bool,
) -> Result<Vec<u8>, Error> {
    let matcher = RegexMatcher::new(regex)?;
    let mut printer = JSON::new(vec![]);

    for log in logs {
        let path = log_path.join(log.path());
        for p in WalkDir::new(path).follow_links(true) {
            let file_path = p?.into_path();
            if file_path.is_dir() {
                continue;
            }

            if unzip {
                let file = File::open(&file_path)?;
                let gz = GzDecoder::new(file);

                if gz.header().is_some() {
                    Searcher::new().search_reader(
                        &matcher,
                        gz,
                        printer.sink_with_path(&matcher, &file_path),
                    )?;
                } else {
                    let mut file = gz.into_inner();

                    // Return to start of file to recover the chunk gz already
                    // consumed checking for headers.
                    file.rewind()?;
                    Searcher::new().search_file(
                        &matcher,
                        &file,
                        printer.sink_with_path(&matcher, &file_path),
                    )?;
                }
            } else {
                // Skip common gzip extensions if we're not decompressing.
                match file_path.extension().and_then(|e| e.to_str()) {
                    Some("gz") | Some("s") => continue,
                    _ => {}
                }

                Searcher::new().search_path(
                    &matcher,
                    &file_path,
                    printer.sink_with_path(&matcher, &file_path),
                )?;
            }
        }
    }

    if printer.has_written() {
        Ok(printer.into_inner())
    } else {
        Err(anyhow!("No matches found"))
    }
}
